package pr.util;

import javafx.geometry.Rectangle2D;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public enum Sprite {
    TILESET("/resources/images/sheet_tiles.bmp", 16),
    PLAYER_SHEET("/resources/images/sheet_smallmariowalk.bmp", 4),
    ENEMY_SHEET("/resources/images/sheet_enemy.bmp", 1);

    private final Image image;
    private final double numberOfFrames;

    private Sprite(String path, int numberOfFrames) {
        image = new Image(getClass().getResource(path).toExternalForm());
        this.numberOfFrames = numberOfFrames;
    }

    public Image getImage() {
        return image;
    }

    public Rectangle2D getViewport(double frame) {
        return new Rectangle2D((image.getWidth() / numberOfFrames)*frame, 0, image.getWidth() / numberOfFrames, image.getHeight());
    }

    public double getNumberOfFrames() {
        return numberOfFrames;
    }
}
