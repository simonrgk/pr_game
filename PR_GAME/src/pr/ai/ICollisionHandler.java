package pr.ai;

import pr.go.Enemy;
import pr.go.GameObject;
import pr.go.Player;

public interface ICollisionHandler {
    public void handleCollisionWithPlayer(Player anotherGo);
    public void handleCollisionWithEnemy(Enemy anotherGo);
}