package pr.ai;

import pr.go.Enemy;
import pr.go.GameObject;
import pr.go.Player;
import pr.World;

public class PlayerAi extends GameObjectAi {
	// TODO: Logika przy użyciu State pattern czy coś.
    
    private boolean jumping = false;
    
	public static final double WALK_SPEED = 100.0;
	
	public PlayerAi(GameObject go) {
		super(go);
	}

	@Override
	public void initialize() {
		super.initialize();
		jumping = false;
	}
    
    
    @Override
    public void handleCollisionWithEnemy(Enemy anotherGo) {
        System.out.println("Player collided with Enemy");
        go.setAlive(false);
    }
    
    /*@Override
    public void handleCollisionWithPlayer(Player anotherGo) {
        System.out.println("Player collided with Player");
    }*/
    
	@Override
	public void handleInput() {
		World w = World.getInstance();

		goingLeft = w.pressingLeft && !w.pressingRight;
		goingRight = w.pressingRight && !w.pressingLeft;
		goingUp = w.pressingUp;
	}


	@Override
	public void update(double dt) {
		handleInput();

		go.setVx(0.0); go.setAx(0.0); go.setAy(GRAVITY_ACCELERATION);
		if(goingLeft || goingRight) {
			go.setVx((go.isFlippedHorizontal() ? -1.0 : 1.0) * WALK_SPEED);
		}
		if(goingUp && collidedBottom) {
		    jumping = true;
			go.setVy(-100);
		}
		if(collidedBottom) jumping = false;
		
		updatePosition(dt);

		if(go.getY() > 200) {
			go.setY(200);
			//go.setAlive(false);
		}


		updateAnimation(dt);
	}
	
	@Override
	public void updateAnimation(double dt) {
        if(goingLeft) {
            go.setFlippedHorizontal(true);
        } else if(goingRight) {
            go.setFlippedHorizontal(false);
        }
        
		if(jumping) {
		    go.setAnimationName("jumping");
		} else if(!goingLeft && !goingRight) {
		    go.setAnimationName("idle");
		} else {
		    go.setAnimationName("walking");
		}
	}
	
}
