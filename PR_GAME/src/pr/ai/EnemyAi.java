package pr.ai;

import pr.go.Enemy;
import pr.go.GameObject;
import pr.go.Player;
import pr.World;

public class EnemyAi extends GameObjectAi {

	public static final double WALK_SPEED = 30.0;
	public EnemyAi(GameObject go) {
		super(go);
	}

	@Override
	public void initialize() {
		super.initialize();
		go.setVx(WALK_SPEED);
	}
	
    @Override
    public void handleCollisionWithEnemy(Enemy anotherGo) {
        System.out.println("Enemy collided with Enemy");
    }
    
	@Override
	public void handleInput() {
		World w = World.getInstance();
		// ...
	}

	@Override
	public void update(double dt) {
		handleInput();

		go.setAx(0.0); go.setAy(GRAVITY_ACCELERATION);
		if(collidedSides) {
			goingLeft = !goingLeft;
		}
		if(collidedBottom) {
			go.setVx((goingLeft ? -1.0 : 1.0) * WALK_SPEED);
		} else {
			go.setVx(0.0);
		}

		updatePosition(dt);

		if(go.getY() > World.getInstance().getMap().getHeight()) {
			go.setAlive(false);
			System.out.println("chuj");
		}


		updateAnimation(dt);
	}

	public void updateAnimation(double dt) {
	    if(!collidedBottom) {
	        go.setAnimationName("idle");
	    } else {
	        go.setAnimationName("walking");
	    }
	}
}
