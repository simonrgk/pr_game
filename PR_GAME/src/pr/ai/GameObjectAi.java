package pr.ai;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import pr.World;
import pr.go.Enemy;
import pr.go.GameObject;
import pr.go.Player;

public abstract class GameObjectAi implements ICollisionHandler {

	protected GameObject go;
	protected boolean collidedBottom, collidedSides, collidedTop;

	protected boolean goingLeft, goingRight, goingUp; // changed by player's input or Ai.
	
	
	public static final double GRAVITY_ACCELERATION = 150.0;

	public GameObjectAi(GameObject go) {
		this.go = go;
	}

	public void initialize() {
		collidedBottom = false; collidedSides = false; collidedTop = false;
		goingLeft = false; goingRight = false; goingUp = false;
	}
    
	public void handleInput() {}

	public void updatePosition(double dt) {
		World w = World.getInstance();

		go.setPrevX(go.getX());
		go.setPrevY(go.getY());
		go.setVx(go.getVx() + go.getAx() * dt);
		go.setVy(go.getVy() + go.getAy() * dt);
		go.setX(go.getX() + go.getVx() * dt);
		collidedSides = w.getMap().collideWithSides(go);
		go.setY(go.getY() + go.getVy() * dt);
		collidedBottom = w.getMap().collideWithBottom(go);
		collidedTop = w.getMap().collideWithTop(go);
	}

	public void update(double dt) {}
    
	public void updateAnimation(double dt) {};
	
    @Override
    public void handleCollisionWithPlayer(Player anotherGo) {}

    @Override
    public void handleCollisionWithEnemy(Enemy anotherGo) {}

	// TODO: Serialization.
}
