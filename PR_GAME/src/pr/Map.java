package pr;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.util.Scanner;

import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.image.ImageView;
import pr.go.GameObject;
import pr.util.Sprite;

public class Map {

    private static class Tile {
        public boolean isSolid;
        public double frame;

        Tile(boolean isSolid, double frame) {
            this.isSolid = isSolid;
            this.frame = frame;  
        }
    }


    private Sprite sprite = Sprite.TILESET;
    private Group group = new Group(); // TODO: Zrobić to na Canvas i dać możliwość modyfikacji po załadowaniu.

    private int widthInTiles, heightInTiles;
    private double tileSize;

    private Tile[][] tiles;

    public Map() {
        String path = "/resources/maps/default.txt";
        Scanner scanner;
        try {
            scanner = new Scanner(new File(getClass().getResource(path).toURI()));

            widthInTiles = scanner.nextInt();
            heightInTiles = scanner.nextInt();
            tileSize = sprite.getImage().getHeight();;
            tiles = new Tile[widthInTiles][heightInTiles];

            for (int y = 0; y < heightInTiles; y++) {
                for (int x = 0; x < widthInTiles; x++) {
                    int frame = scanner.nextInt();
                    boolean isSolid = frame < 4 ? false : true;
                    tiles[x][y] = new Tile(isSolid, frame);

                    ImageView imageView = new ImageView(sprite.getImage());
                    imageView.setViewport(sprite.getViewport(frame));
                    imageView.setX(x * tileSize);
                    imageView.setY(y * tileSize);

                    group.getChildren().add(imageView);
                }
            }
        } catch (FileNotFoundException | URISyntaxException e) {
            e.printStackTrace();
            tiles = null;
        }
    }

    public double getWidth() {
        return widthInTiles * tileSize;
    }

    public double getHeight() {
        return heightInTiles * tileSize;
    }

    public final Tile getTile(int x, int y) {
        return tiles[x][y];
    }

    public boolean collideWithBottom(GameObject go) {
        for (int y = Math.max(0, (int)(go.getY() / tileSize) - 2); y < Math.min(heightInTiles, (int)((go.getY() + go.getHeight()) / tileSize) + 2); y++) {
            for (int x = Math.max(0, (int)(go.getX() / tileSize) - 2); x < Math.min(widthInTiles, (int)((go.getX() + go.getWidth()) / tileSize) + 2); x++) {
                if (getTile(x, y).isSolid) { // TODO: tile solidity checking
                    double tileX = x*tileSize;
                    double tileY = y*tileSize;
                    if (Utils.intersects(go.getX() + go.getOffsetLeft(), go.getY() + go.getOffsetTop() + go.getBoundedHeight() - 1, go.getBoundedWidth(), 1, tileX, tileY, tileSize, tileSize)) {
                        go.setY(tileY - tileSize + go.getOffsetBottom());
                        if (go.getVy() > 0.0) go.setVy(0.0); // Stop moving after hitting the top of the tile.
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public boolean collideWithSides(GameObject go) {
        for (int y = Math.max(0, (int)(go.getY() / tileSize) - 2); y < Math.min(heightInTiles, (int)((go.getY() + go.getHeight()) / tileSize) + 2); y++) {
            for (int x = Math.max(0, (int)(go.getX() / tileSize) - 2); x < Math.min(widthInTiles, (int)((go.getX() + go.getWidth()) / tileSize) + 2); x++) {
                if (getTile(x, y).isSolid) { // TODO: tile solidity checking
                    double tileX = x*tileSize;
                    double tileY = y*tileSize;
                    if (Utils.intersects(go.getX() + go.getOffsetLeft(), go.getY() + go.getOffsetTop(), 1, go.getBoundedHeight(), tileX, tileY, tileSize, tileSize)) {
                        // Collided from left.
                        go.setX(Math.floor((int)(go.getX() / tileSize) * tileSize + tileSize) - go.getOffsetLeft());
                        //if (go.getVx() < 0.0) go.setVx(0.0);
                        return true;
                    }
                    if (Utils.intersects(go.getX() + go.getOffsetLeft() + go.getBoundedWidth() - 1, go.getY() + go.getOffsetTop(), 1, go.getBoundedHeight(), tileX, tileY, tileSize, tileSize)) {
                        // Collided from right.
                        go.setX(Math.floor((int)(go.getX() / tileSize) * tileSize) + go.getOffsetRight());
                        //if (go.getVx() > 0.0) go.setVx(0.0);
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public boolean collideWithTop(GameObject go) {
        for (int y = Math.max(0, (int)(go.getY() / tileSize) - 2); y < Math.min(heightInTiles, (int)((go.getY() + go.getHeight()) / tileSize) + 2); y++) {
            for (int x = Math.max(0, (int)(go.getX() / tileSize) - 2); x < Math.min(widthInTiles, (int)((go.getX() + go.getWidth()) / tileSize) + 2); x++) {
                if (getTile(x, y).isSolid) { // TODO: tile solidity checking
                    double tileX = x*tileSize;
                    double tileY = y*tileSize;
                    if (Utils.intersects(go.getX() + go.getOffsetLeft(), go.getY() + go.getOffsetTop(), go.getBoundedWidth(), 1, tileX, tileY, tileSize, tileSize)) {
                        go.setY(tileY + tileSize - go.getOffsetTop());
                        if (go.getVy() < 0.0) go.setVy(0.0); // Stop moving after hitting the bottom of a tile.
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public Node getNode() {
        return group;
    }



}
