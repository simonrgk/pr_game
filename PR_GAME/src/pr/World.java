package pr;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import pr.go.Enemy;
import pr.go.GameObject;
import pr.go.Player;

public class World {
    private ObservableList<GameObject> gameObjects = FXCollections.observableArrayList();
    private Map map = new Map();

    public boolean pressingLeft = false, pressingRight = false, pressingUp = false, pressingDown = false;
    public boolean pressedLeft = false, pressedRight = false, pressedUp = false, pressedDown = false;

    private long currentFrameNumber = 0;

    public void initialize() {
        for(int i = 0; i < Utils.randomInt(2, 5); i++) {
            addGameObject(new Enemy(Utils.randomInt(20, 200), Utils.randomInt(20, 40)));
        }

        addGameObject(new Player());
    }

    public final void addGameObject(GameObject object) {
        gameObjects.add(object);
    }

    public ObservableList<GameObject> getGameObjects() {
        return gameObjects;
    }

    public final void update(double delta) {
        pressedLeft = !pressedLeft && pressingLeft;
        pressedRight = !pressedRight && pressingRight;
        pressedUp = !pressedUp && pressingUp;
        pressedDown = !pressedDown && pressingDown;
        
        gameObjects.forEach(go -> {
            go.clearCollidedWith(); // TODO: Spatial partitioning, tylko że nie trzeba.
            gameObjects.forEach(anotherGo -> {
                if(go.intersectsWith(anotherGo) && go != anotherGo) go.addCollidedWith(anotherGo);
                });
            });
        gameObjects.forEach(go -> go.update(delta));
        gameObjects.removeIf(go -> !go.isAlive());

        currentFrameNumber += 1;
    }

    public Map getMap() {
        return map;
    }

    // TODO: Serialization.

    /* Singleton */

    private static final World instance = new World();

    private World() {}

    public static World getInstance() {
        return instance;
    }
}
