package pr;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.collections.ListChangeListener;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import pr.go.GameObject;

public class GameApplication extends Application {

	public final static int WIDTH = 240;
	public final static int HEIGHT = 240;

	private Pane root;

	private Parent createContent() {
		root = new Pane();
		root.setPrefSize(WIDTH, HEIGHT);

		World.getInstance().getGameObjects().addListener(new ListChangeListener<GameObject>() {
			@Override
			public void onChanged(final Change<? extends GameObject> change) {
				while (change.next()) {
					for (final GameObject o : change.getAddedSubList()) {
						root.getChildren().add(o.getNode());
					}
					for (final GameObject o : change.getRemoved()) {
						//System.out.println("removed");
						root.getChildren().remove(o.getNode());
					}
				}
			}
		});
		root.getChildren().add(World.getInstance().getMap().getNode());

		World.getInstance().initialize();
		
		Label fpsLabel = new Label();
		root.getChildren().add(fpsLabel);
		
		final AnimationTimer loop = new AnimationTimer() {
			private static final double timeStep = 1.0 / 60.0;
			private long previousTime = 0;
			private double accumulatedTime = 0;
			private double elapsedTimeSinceLastFpsUpdate = 0.0;
			private int framesSinceLastFpsUpdate = 0;
			
			// Fixed time steps
			@Override
			public void handle(final long currentTime) {
				if (previousTime == 0) {
					previousTime = currentTime;
					return;
				}

				double elapsedTime = (currentTime - previousTime) / 1.0e9; // from nanoseconds to seconds
				accumulatedTime += elapsedTime;
				previousTime = currentTime;

				while (accumulatedTime >= timeStep) {
					update(timeStep);
					accumulatedTime -= timeStep;
				}
				
				elapsedTimeSinceLastFpsUpdate += elapsedTime;
				framesSinceLastFpsUpdate++;
				if(elapsedTimeSinceLastFpsUpdate > 0.5) {
				    int fps = (int)Math.floor(framesSinceLastFpsUpdate / elapsedTimeSinceLastFpsUpdate);
	                fpsLabel.setText(String.format("FPS: %.3f", 1.0 / elapsedTime));
	                elapsedTimeSinceLastFpsUpdate = 0.0;
	                framesSinceLastFpsUpdate = 0;
				}
			}
		};
		loop.start();

		return root;
	}

	private void update(final double dt) {
		World.getInstance().update(dt);
	}

	// TODO: Zrobić jakoś bez eventów, bo nie wiadomo jak to z pętlą główną współpracuje.
	public void handleKeyPressed(final KeyEvent key) {
		final World w = World.getInstance();

		if(key.getCode() == KeyCode.LEFT) {
			w.pressingLeft = true;
		} else if(key.getCode() == KeyCode.RIGHT) {
			w.pressingRight = true;
		} else if(key.getCode() == KeyCode.UP) {
			w.pressingUp = true;
		} else if(key.getCode() == KeyCode.DOWN) {
			w.pressingDown = true;
		}
	}

	public void handleKeyReleased(final KeyEvent key) {
		final World w = World.getInstance();

		if(key.getCode() == KeyCode.LEFT) {
			w.pressingLeft = false;
		} else if(key.getCode() == KeyCode.RIGHT) {
			w.pressingRight = false;
		} else if(key.getCode() == KeyCode.UP) {
			w.pressingUp = false;
		} else if(key.getCode() == KeyCode.DOWN) {
			w.pressingDown = false;
		}
	} 

	@Override
	public void start(final Stage stage) throws Exception {
		final Scene scene = new Scene(createContent());
		scene.setOnKeyPressed(this::handleKeyPressed);
		scene.setOnKeyReleased(this::handleKeyReleased);
		stage.setTitle("Projekt PR");
		stage.setScene(scene);
		stage.show();

	}

	public static void main(final String[] args) {
		launch(args);
	}


}
