package pr.go;

import pr.go.GameObject;
import pr.ai.PlayerAi;
import pr.util.Sprite;

public class Player extends GameObject {

    public Player() {
        super(Sprite.PLAYER_SHEET);
        setBounds(1, 1, 0, 1);
        setAi(new PlayerAi(this));
        getAi().initialize();
    }
    
    @Override
    public void collideWith(GameObject anotherGo) {
        anotherGo.getAi().handleCollisionWithPlayer(this);
    }
    
    @Override
    public void updateAnimation(double dt) {
        if(getAnimationName().equals("jumping")) {// TODO: Animacja skoku.
        } if(getAnimationName().equals("idle")) {
            setFrame(0);
        } else if(getAnimationName().equals("walking")) {
            animationCounter += dt;
            if(animationCounter < 0.2) {
                setFrame(1);
            } else if(animationCounter < 0.4) {
                setFrame(2);
            } else if(animationCounter < 0.6) {
                setFrame(3);
            } else if(animationCounter < 0.8) {
                setFrame(2);
            } else {
                animationCounter = 0.0;
            }
        } else {
            setFrame(0);
        }
    }
}