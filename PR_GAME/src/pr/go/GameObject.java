package pr.go;

import java.util.LinkedList;
import java.util.List;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.geometry.Point2D;
import javafx.scene.Node;
import javafx.scene.image.ImageView;
import javafx.scene.transform.Transform;
import pr.Utils;
import pr.ai.GameObjectAi;
import pr.util.Sprite;

public class GameObject {

    private final ImageView node = new ImageView();
    private Sprite sprite;
    private boolean flippedHorizontal = false;
    private double frame = 0;
    protected double animationCounter = 0.0;
    protected String animationName = "idle";

    private double offsetLeft = 0.0, offsetRight = 0.0, offsetBottom = 0.0, offsetTop = 0.0;

    private final DoubleProperty property_x = new SimpleDoubleProperty(0);
    private final DoubleProperty property_y = new SimpleDoubleProperty(0);

    private double prevX = 0.0, prevY = 0.0;
    private double x = 0.0, y = 0.0;
    private double vx = 0.0, vy = 0.0;
    private double ax = 0.0, ay = 0.0;

    private boolean isAlive = true;

    private GameObjectAi ai = null;
    
    private List<GameObject> collidedWith = new LinkedList<GameObject>();
    

    public GameObject(Sprite sprite) {
        setSprite(sprite);
        setBounds(0, 0, 0, 0);
    }
    
    public void clearCollidedWith() {
        collidedWith.clear();
    }
    
    public void addCollidedWith(GameObject go) {
        collidedWith.add(go);
    }
    
    public void collideWith(GameObject anotherGo) {}
    
    public void update(double dt) {
        for(GameObject go : collidedWith) {
            go.collideWith(this);
        }
        ai.update(dt);
        updateAnimation(dt);
    }
    
    public void updateAnimation(double dt) {}
    
    public GameObjectAi getAi() {
        return ai;
    }

    public void setAi(GameObjectAi ai) {
        this.ai = ai;
    }

    public final void setSprite(Sprite sprite) {
        this.sprite = sprite;
        node.setImage(sprite.getImage());
        setFrame(0);

        node.translateXProperty().unbind();
        node.translateXProperty().bind(property_x); // anchor point is top-left

        node.translateYProperty().unbind();
        node.translateYProperty().bind(property_y);


        setFlippedHorizontal(flippedHorizontal);	
    }

    public Sprite getSprite() {
        return sprite;
    }

    public double getWidth() {
        return sprite.getImage().getWidth() / sprite.getNumberOfFrames();
    }

    public double getHeight() {
        return sprite.getImage().getHeight();
    };

    public double getOffsetLeft() {
        return offsetLeft;
    }

    public void setOffsetLeft(double offsetLeft) {
        this.offsetLeft = offsetLeft;
    }

    public double getOffsetRight() {
        return offsetRight;
    }

    public void setOffsetRight(double offsetRight) {
        this.offsetRight = offsetRight;
    }

    public double getOffsetBottom() {
        return offsetBottom;
    }

    public void setOffsetBottom(double offsetBottom) {
        this.offsetBottom = offsetBottom;
    }

    public double getOffsetTop() {
        return offsetTop;
    }

    public void setOffsetTop(double offsetTop) {
        this.offsetTop = offsetTop;
    }

    public void setBounds(double left, double right, double bottom, double top) {
        setOffsetLeft(left);
        setOffsetRight(right);
        setOffsetBottom(bottom);
        setOffsetTop(top);
    }

    public double getBoundedWidth() {
        return getWidth() - getOffsetLeft() - getOffsetRight();
    }

    public double getBoundedHeight() {
        return getHeight() - getOffsetBottom() - getOffsetTop();
    };

    private final void clearTransforms() {
        node.getTransforms().clear();
        flippedHorizontal = false;
    }

    public boolean isFlippedHorizontal() {
        return flippedHorizontal;
    }

    public void setFlippedHorizontal(boolean flip) {
        if(flip && !flippedHorizontal) {
            node.getTransforms().add(Transform.scale(-1, 1, node.getBoundsInLocal().getWidth() / 2, node.getBoundsInLocal().getHeight() / 2));
            flippedHorizontal = true;
        } else if(!flip && flippedHorizontal){
            clearTransforms(/* ... */); // TODO: Czyszczenie transformacji node tylko z Transform.scale.
        }
    }

    public boolean isAlive() {
        return isAlive;
    }

    public void setAlive(boolean isAlive) {
        this.isAlive = isAlive;
    }

    public ImageView getNode() {
        return node;
    }

    public double getFrame() {
        return frame;
    }

    public void setFrame(double frame) {
        this.frame = frame;
        node.setViewport(sprite.getViewport(frame));
    }
    
    public String getAnimationName() {
        return animationName;
    }

    public void setAnimationName(String animationName) {
        this.animationName = animationName;
    }
    
    public double getPrevX() {
        return prevX;
    }

    public void setPrevX(double prevX) {
        this.prevX = prevX;
    }

    public double getPrevY() {
        return prevY;
    }

    public void setPrevY(double prevY) {
        this.prevY = prevY;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
        property_x.set(this.x);
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
        property_y.set(this.y);
    }

    public double getVx() {
        return vx;
    }

    public void setVx(double vx) {
        this.vx = vx;
    }

    public double getVy() {
        return vy;
    }

    public void setVy(double vy) {
        this.vy = vy;
    }

    public double getAx() {
        return ax;
    }

    public void setAx(double ax) {
        this.ax = ax;
    }

    public double getAy() {
        return ay;
    }

    public void setAy(double ay) {
        this.ay = ay;
    }
    
    public boolean intersectsWith(GameObject anotherGo) {
        return GameObject.intersects(this, anotherGo);
    }
    
    public static boolean intersects(GameObject go1, GameObject go2) {
        return Utils.intersects(go1.getX(), go1.getY(), go1.getWidth(), go1.getHeight(), go2.getX(), go2.getY(), go2.getWidth(), go2.getHeight());

    }

}
