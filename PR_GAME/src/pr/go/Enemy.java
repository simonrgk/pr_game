package pr.go;

import pr.ai.EnemyAi;
import pr.ai.PlayerAi;
import pr.util.Sprite;

public class Enemy extends GameObject {

	public Enemy(double x, double y) {
		super(Sprite.ENEMY_SHEET);
		setFrame(0);
		setBounds(1, 1, 0, 1);
		setX(x); setY(y);
		setAi(new EnemyAi(this));
		getAi().initialize();
	}
	
    @Override
    public void collideWith(GameObject anotherGo) {
        anotherGo.getAi().handleCollisionWithEnemy(this);
    }
    
    public void updateAnimation(double dt) {
        if(getAnimationName().equals("idle")) {
            // Don't move...
        } else if(getAnimationName().equals("walking")) {
            animationCounter += dt;
            if(animationCounter > 0.2) {
                setFlippedHorizontal(!isFlippedHorizontal());
                animationCounter = 0.0;
            }
        } else {
            setFrame(0);
        }
    }
}